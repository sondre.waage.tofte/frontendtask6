import '../styles/index.scss';
import { ENETUNREACH } from 'constants';

var apiKey = "563492ad6f91700001000001e02b600f5f66401a8b00a3222bbea246";
var search = "People";
var images = "15";
var apiUrl = "https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=" + search + "&per_page=" + images + "15&page=1";

// Render all the photos from the API
// Keeping reused elements

function renderPhotos(photos) {
    $.each(photos, (i, photo)=>{
    $('#row').append(createCard(photo));
    });
   } 

// Sondre create card function
   function createCard(photo) {
     return `
     <div class="col-3">
     <div class="card h-100" style="18rem;">
     <div class="card-img">
     <img class="card-img-top" src="${photo.src.medium}"/>
     </div>
    
     <div class="card-body">
     <h5>${photo.photographer}</h5>
     </div>
     </div>
     </div>
     `;
    }

function ajaxcall()
{
    $.ajax({
        type: 'GET',
        headers: {
        "Authorization": `Bearer ${apiKey}`
        },
        url: apiUrl
    }).done(data => {
        renderPhotos(data.photos);
    });
}

function ChangeImages()
{
    search = document.getElementById("txt").value;
    if(search === "")
    {
        search=lions;
    }
    images = document.getElementById("img").value;
    apiUrl = "https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=" + search + "&per_page=" + images + "&page=1";
    $("#row").empty();
    ajaxcall();
}
global.ChangeImages = ChangeImages;

$(document).ready(()=>{
    ajaxcall();
    console.log('Hello jQuery');
});

window.helloWorld = function() {
    console.log('HEllooOooOOo!');
};
